package com.itworx.chat.bot.repository;

import com.itworx.chat.bot.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<Message, Integer> {

    Message findMessageByMessageLikeIgnoreCase(String message);

}
