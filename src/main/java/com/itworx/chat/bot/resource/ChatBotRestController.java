package com.itworx.chat.bot.resource;

import com.itworx.chat.bot.control.BotReplyService;
import com.itworx.chat.bot.control.BotValidationService;
import com.itworx.chat.bot.model.ChatBotResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api")
public class ChatBotRestController {

  private final BotReplyService botReplyService;
  private final BotValidationService botValidationService;

  @Autowired
  public ChatBotRestController(
      BotReplyService botReplyService, BotValidationService botValidationService) {
    this.botReplyService = botReplyService;
    this.botValidationService = botValidationService;
  }

  @GetMapping("reply/{bot-id}")
  public ResponseEntity<ChatBotResponse> getReply(
      @PathVariable("bot-id") Integer botId, @RequestParam(value = "message") String message) {
    if (Boolean.TRUE.equals(botValidationService.validateBotId(botId))) {
      ChatBotResponse reply = botReplyService.getReply(message);
      return ResponseEntity.ok(reply);
    } else {
      return ResponseEntity.status(HttpStatus.NOT_FOUND)
          .body(new ChatBotResponse(String.format("Failed to Recognize bot with id %s", botId)));
    }
  }
}
