package com.itworx.chat.bot.exception;

public class IntentReplyNotFoundException extends Exception {

    public IntentReplyNotFoundException() {
    }

    public IntentReplyNotFoundException(String message) {
        super(message);
    }
}
