package com.itworx.chat.bot.exception;

public class IntentRecognitionFailedException extends Exception {

    public IntentRecognitionFailedException() {
    }

    public IntentRecognitionFailedException(String message) {
        super(message);
    }
}
