package com.itworx.chat.bot.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "INTENT_MESSAGE")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class IntentMessage {

    @Id
    @Column(name = "intent_message_id")
    @GeneratedValue
    private int intentMessageId;

    @ManyToOne
    @JoinColumn(name = "intent_id", referencedColumnName = "intent_id")
    private Intent intent;

    @ManyToOne
    @JoinColumn(name = "message_id", referencedColumnName = "message_id")
    private Message message;

    private float confidence;
}
