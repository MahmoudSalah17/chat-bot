package com.itworx.chat.bot.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "REPLY")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Reply {

  @Id
  @Column(name = "reply_id")
  @GeneratedValue
  private int replyId;

  @Column(name = "REPLY_MESSAGE")
  private String replyMessage;

  @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "reply")
  private Intent intent;
}
