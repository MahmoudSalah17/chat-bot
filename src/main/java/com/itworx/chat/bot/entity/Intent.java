package com.itworx.chat.bot.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "INTENT")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Intent {

    @Id
    @Column(name = "intent_id")
    @GeneratedValue
    private int intentId;

    @Column(name = "name")
    private String name;

    @OneToOne(fetch =FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "reply_id")
    private Reply reply;

    @OneToMany(mappedBy = "intent")
    private List<IntentMessage> messages;
}
