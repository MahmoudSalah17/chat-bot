package com.itworx.chat.bot.control;

import com.itworx.chat.bot.entity.IntentMessage;
import com.itworx.chat.bot.exception.IntentRecognitionFailedException;
import com.itworx.chat.bot.exception.IntentReplyNotFoundException;
import com.itworx.chat.bot.model.ChatBotResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

// This is the business layer for the API
@Service
@Slf4j
public class BotReplyService {

  private final IntentRecognizerService intentRecognizerService;
  private final float threshold;

  @Autowired
  public BotReplyService(
      @Value("${threshold}") float threshold, IntentRecognizerService intentRecognizerService) {
    this.threshold = threshold;
    this.intentRecognizerService = intentRecognizerService;
  }

  public ChatBotResponse getReply(String messageStr) {
    try {
      List<IntentMessage> intents = intentRecognizerService.getIntent(messageStr);
      IntentMessage intentMessageFiltered = getIntentMessageFiltered(intents);
      return new ChatBotResponse(getReply(intentMessageFiltered));
    } catch (IntentRecognitionFailedException | IntentReplyNotFoundException e) {
      log.error(e.getMessage(), e);
      return new ChatBotResponse("Sorry, I can't understand you");
    }
  }

  private IntentMessage getIntentMessageFiltered(List<IntentMessage> intents)
      throws IntentRecognitionFailedException {
    return intents.stream()
        .filter(intentMessage -> intentMessage.getConfidence() >= threshold)
        .max(Comparator.comparing(IntentMessage::getConfidence))
        .orElseThrow(IntentRecognitionFailedException::new);
  }

  private String getReply(IntentMessage intentMessage) throws IntentReplyNotFoundException {
    if (intentMessage.getIntent() != null && intentMessage.getIntent().getReply() != null) {
      return intentMessage.getIntent().getReply().getReplyMessage();
    } else {
      throw new IntentReplyNotFoundException(
          String.format("Failed to Retrieve Reply for %s:", intentMessage.getIntent().getName()));
    }
  }
}
