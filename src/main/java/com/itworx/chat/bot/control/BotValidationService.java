package com.itworx.chat.bot.control;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

// Assuming we have multiple bots with Ids and we need to validate each ID
@Service
@Slf4j
public class BotValidationService {

  public static final Integer BOT_ID = 1234;

  public Boolean validateBotId(Integer botId) {
    return BOT_ID.equals(botId);
  }
}
