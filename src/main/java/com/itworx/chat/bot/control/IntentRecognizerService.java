package com.itworx.chat.bot.control;

import com.itworx.chat.bot.entity.IntentMessage;
import com.itworx.chat.bot.entity.Message;
import com.itworx.chat.bot.exception.IntentRecognitionFailedException;
import com.itworx.chat.bot.repository.MessageRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// This layer should represent the bot itself and how it is able to recognize the intent of the
// message

@Service
@Slf4j
public class IntentRecognizerService {

  private final MessageRepository messageRepository;

  @Autowired
  public IntentRecognizerService(MessageRepository messageRepository) {
    this.messageRepository = messageRepository;
  }

  public List<IntentMessage> getIntent(String messageStr) throws IntentRecognitionFailedException {
    Message message = messageRepository.findMessageByMessageLikeIgnoreCase(messageStr);
    if (message != null) {
      List<IntentMessage> intents = message.getIntents();
      if (intents != null && !intents.isEmpty()) {
        return intents;
      } else {
        throw new IntentRecognitionFailedException(
            String.format("Failed to Recognized with message : %s", messageStr));
      }
    } else {
      throw new IntentRecognitionFailedException(
          String.format("Failed to Recognized with message : %s", messageStr));
    }
  }
}
