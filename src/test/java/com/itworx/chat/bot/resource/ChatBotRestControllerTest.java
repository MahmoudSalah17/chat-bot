package com.itworx.chat.bot.resource;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

import com.itworx.chat.bot.control.BotReplyService;
import com.itworx.chat.bot.control.BotValidationService;
import com.itworx.chat.bot.model.ChatBotResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {ChatBotRestController.class})
@ExtendWith(SpringExtension.class)
class ChatBotRestControllerTest {
    @Autowired
    private ChatBotRestController chatBotRestController;

    @MockBean
    private BotValidationService botValidationService;
    @MockBean
    private BotReplyService botReplyService;

    @Test
    void testGetReply() throws Exception {
        when(botReplyService.getReply(any())).thenReturn(new ChatBotResponse("Reply Message"));
        when(botValidationService.validateBotId(any())).thenReturn(true);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/reply/{bot-id}", "1234")
                .param("message", "foo");
        MockMvcBuilders.standaloneSetup(chatBotRestController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("{\"replyMessage\":\"Reply Message\"}"));
    }

    @Test
    void testGetReply2() throws Exception {
        when(botReplyService.getReply(any())).thenReturn(new ChatBotResponse("Reply Message"));
        when(botValidationService.validateBotId(any())).thenReturn(false);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/reply/{bot-id}", "123")
                .param("message", "foo");
        MockMvcBuilders.standaloneSetup(chatBotRestController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("{\"replyMessage\":\"Failed to Recognize bot with id 123\"}"));
    }
}

