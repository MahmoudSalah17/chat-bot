package com.itworx.chat.bot.control;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {BotValidationService.class})
@ExtendWith(SpringExtension.class)
class BotValidationServiceTest {
    @Autowired
    private BotValidationService botValidationService;

    @Test
    void testValidateBotId() {
        assertFalse(botValidationService.validateBotId(123));
        assertTrue(botValidationService.validateBotId(1234));
    }
}

