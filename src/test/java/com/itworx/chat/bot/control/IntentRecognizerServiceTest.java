package com.itworx.chat.bot.control;

import com.itworx.chat.bot.entity.Intent;
import com.itworx.chat.bot.entity.IntentMessage;
import com.itworx.chat.bot.entity.Message;
import com.itworx.chat.bot.entity.Reply;
import com.itworx.chat.bot.exception.IntentRecognitionFailedException;
import com.itworx.chat.bot.repository.MessageRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {IntentRecognizerService.class})
@ExtendWith(SpringExtension.class)
class IntentRecognizerServiceTest {
  @Autowired private IntentRecognizerService intentRecognizerService;

  @MockBean private MessageRepository messageRepository;

  @Test
  void testGetIntent() {
    Message message = new Message();
    message.setIntents(new ArrayList<>());
    message.setMessage("Not all who wander are lost");
    message.setMessageId(123);
    when(messageRepository.findMessageByMessageLikeIgnoreCase(any())).thenReturn(message);
    assertThrows(
        IntentRecognitionFailedException.class,
        () -> intentRecognizerService.getIntent("Message Str"));
    verify(messageRepository).findMessageByMessageLikeIgnoreCase(any());
  }

  @Test
  void testGetIntent2() throws IntentRecognitionFailedException {
    Intent intent = new Intent();
    intent.setIntentId(123);
    intent.setMessages(new ArrayList<>());
    intent.setName("intent");
    intent.setReply(new Reply());

    Reply reply = new Reply();
    reply.setIntent(intent);
    reply.setReplyId(123);
    reply.setReplyMessage("intent");

    Intent intent1 = new Intent();
    intent1.setIntentId(123);
    intent1.setMessages(new ArrayList<>());
    intent1.setName("intent");
    intent1.setReply(reply);

    Message message = new Message();
    message.setIntents(new ArrayList<>());
    message.setMessage("Not all who wander are lost");
    message.setMessageId(123);

    IntentMessage intentMessage = new IntentMessage();
    intentMessage.setConfidence(0.95f);
    intentMessage.setIntent(intent1);
    intentMessage.setIntentMessageId(123);
    intentMessage.setMessage(message);

    ArrayList<IntentMessage> intentMessageList = new ArrayList<>();
    intentMessageList.add(intentMessage);

    Message message1 = new Message();
    message1.setIntents(intentMessageList);
    message1.setMessage("Not all who wander are lost");
    message1.setMessageId(123);
    when(messageRepository.findMessageByMessageLikeIgnoreCase(any())).thenReturn(message1);
    List<IntentMessage> actualIntent = intentRecognizerService.getIntent("Message Str");
    assertSame(intentMessageList, actualIntent);
    assertEquals(1, actualIntent.size());
    verify(messageRepository).findMessageByMessageLikeIgnoreCase(any());
  }
}
