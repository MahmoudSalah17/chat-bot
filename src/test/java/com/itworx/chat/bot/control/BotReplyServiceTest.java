package com.itworx.chat.bot.control;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.itworx.chat.bot.entity.Intent;
import com.itworx.chat.bot.entity.IntentMessage;
import com.itworx.chat.bot.entity.Message;
import com.itworx.chat.bot.entity.Reply;
import com.itworx.chat.bot.repository.MessageRepository;

import java.util.ArrayList;
import java.util.Collections;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;


//Auto generated and will remove it afterwards
class BotReplyServiceTest {

  @SneakyThrows
  @Test
  void testGetReplyHappyScenario() {

    Message message = new Message();
    message.setIntents(new ArrayList<>());
    message.setMessage("Not all who wander are lost");
    message.setMessageId(123);
    IntentRecognizerService intentRecognizerService = mock(IntentRecognizerService.class);
    IntentMessage intentMessage = new IntentMessage();
    Intent intent = new Intent();
    Reply reply = new Reply();
    reply.setReplyMessage("Test Reply");
    intent.setReply(reply);
    intentMessage.setIntent(intent);
    intentMessage.setConfidence(0.8f);
    when(intentRecognizerService.getIntent(any())).thenReturn(Collections.singletonList(intentMessage));
    assertEquals(
        "Test Reply",
        (new BotReplyService(0.5f, intentRecognizerService))
            .getReply("Message Str")
            .getReplyMessage());
    verify(intentRecognizerService).getIntent(any());
  }

  @SneakyThrows
  @Test
  void testGetReplyAboveThreshold() {

    Intent intent = new Intent();
    intent.setIntentId(123);
    intent.setMessages(new ArrayList<>());
    intent.setName("Intent");
    intent.setReply(new Reply());

    Reply reply = new Reply();
    reply.setIntent(intent);
    reply.setReplyId(123);
    reply.setReplyMessage("Test Reply");

    Intent intent1 = new Intent();
    intent1.setIntentId(123);
    intent1.setMessages(new ArrayList<>());
    intent1.setName("Intent");
    intent1.setReply(reply);

    Message message = new Message();
    message.setIntents(new ArrayList<>());
    message.setMessage("Not all who wander are lost");
    message.setMessageId(123);

    IntentMessage intentMessage = new IntentMessage();
    intentMessage.setConfidence(0.95f);
    intentMessage.setIntent(intent1);
    intentMessage.setIntentMessageId(123);
    intentMessage.setMessage(message);

    ArrayList<IntentMessage> intentMessageList = new ArrayList<>();
    intentMessageList.add(intentMessage);

    Message message1 = new Message();
    message1.setIntents(intentMessageList);
    message1.setMessage("Not all who wander are lost");
    message1.setMessageId(123);
    IntentRecognizerService intentRecognizerService = mock(IntentRecognizerService.class);
    when(intentRecognizerService.getIntent(any())).thenReturn(intentMessageList);
    assertEquals(
        "Test Reply",
        (new BotReplyService(0.5f, intentRecognizerService))
            .getReply("Message Str")
            .getReplyMessage());
    verify(intentRecognizerService).getIntent(any());
  }

  @SneakyThrows
  @Test
  void testGetReplyBelowThreshold() {

    Intent intent = new Intent();
    intent.setIntentId(123);
    intent.setMessages(new ArrayList<>());
    intent.setName("Intent");
    intent.setReply(new Reply());

    Reply reply = new Reply();
    reply.setIntent(intent);
    reply.setReplyId(123);
    reply.setReplyMessage("Test Reply");

    Intent intent1 = new Intent();
    intent1.setIntentId(123);
    intent1.setMessages(new ArrayList<>());
    intent1.setName("Intent");
    intent1.setReply(reply);

    Message message = new Message();
    message.setIntents(new ArrayList<>());
    message.setMessage("Not all who wander are lost");
    message.setMessageId(123);

    IntentMessage intentMessage = new IntentMessage();
    intentMessage.setConfidence(0.7f);
    intentMessage.setIntent(intent1);
    intentMessage.setIntentMessageId(123);
    intentMessage.setMessage(message);

    ArrayList<IntentMessage> intentMessageList = new ArrayList<>();
    intentMessageList.add(intentMessage);

    Message message1 = new Message();
    message1.setIntents(intentMessageList);
    message1.setMessage("Not all who wander are lost");
    message1.setMessageId(123);
    IntentRecognizerService intentRecognizerService = mock(IntentRecognizerService.class);
    when(intentRecognizerService.getIntent(any())).thenReturn(intentMessageList);
    assertEquals(
        "Sorry, I can't understand you",
        (new BotReplyService(0.8f, intentRecognizerService))
            .getReply("Message Str")
            .getReplyMessage());
    verify(intentRecognizerService).getIntent(any());
  }

  @SneakyThrows
  @Test
  void testGetReplyMultipleIntentForOneMessageFilter() {

    Intent intent = new Intent();
    intent.setIntentId(123);
    intent.setMessages(new ArrayList<>());
    intent.setName("Sorry, I can't understand you");
    intent.setReply(new Reply());

    Reply reply = new Reply();
    reply.setIntent(intent);
    reply.setReplyId(123);
    reply.setReplyMessage("Higher Confidence");

    Intent intent1 = new Intent();
    intent1.setIntentId(123);
    intent1.setMessages(new ArrayList<>());
    intent1.setName("Sorry, I can't understand you");
    intent1.setReply(reply);

    Message message = new Message();
    message.setIntents(new ArrayList<>());
    message.setMessage("Not all who wander are lost");
    message.setMessageId(123);

    IntentMessage intentMessage = new IntentMessage();
    intentMessage.setConfidence(0.95f);
    intentMessage.setIntent(intent1);
    intentMessage.setIntentMessageId(123);
    intentMessage.setMessage(message);

    Intent intent2 = new Intent();
    intent2.setIntentId(123);
    intent2.setMessages(new ArrayList<>());
    intent2.setName("Name");
    intent2.setReply(new Reply());

    Reply reply1 = new Reply();
    reply1.setIntent(intent2);
    reply1.setReplyId(123);
    reply1.setReplyMessage("Lower Confidence");

    Intent intent3 = new Intent();
    intent3.setIntentId(123);
    intent3.setMessages(new ArrayList<>());
    intent3.setName("Name");
    intent3.setReply(reply1);

    Message message1 = new Message();
    message1.setIntents(new ArrayList<>());
    message1.setMessage("Not all who wander are lost");
    message1.setMessageId(123);

    IntentMessage intentMessage1 = new IntentMessage();
    intentMessage1.setConfidence(0.4f);
    intentMessage1.setIntent(intent3);
    intentMessage1.setIntentMessageId(123);
    intentMessage1.setMessage(message1);

    ArrayList<IntentMessage> intentMessageList = new ArrayList<>();
    intentMessageList.add(intentMessage1);
    intentMessageList.add(intentMessage);

    Message message2 = new Message();
    message2.setIntents(intentMessageList);
    message2.setMessage("Not all who wander are lost");
    message2.setMessageId(123);
    IntentRecognizerService intentRecognizerService = mock(IntentRecognizerService.class);
    when(intentRecognizerService.getIntent(any())).thenReturn(intentMessageList);
    assertEquals(
        "Higher Confidence",
        (new BotReplyService(0.5f, intentRecognizerService))
            .getReply("Message Str")
            .getReplyMessage());
    verify(intentRecognizerService).getIntent(any());
  }
@SneakyThrows
  @Test
  void testGetReplyMultipleIntentForOneMessageFilter2() {

    Intent intent = new Intent();
    intent.setIntentId(123);
    intent.setMessages(new ArrayList<>());
    intent.setName("Sorry, I can't understand you");
    intent.setReply(new Reply());

    Reply reply = new Reply();
    reply.setIntent(intent);
    reply.setReplyId(123);
    reply.setReplyMessage("Higher Confidence");

    Intent intent1 = new Intent();
    intent1.setIntentId(123);
    intent1.setMessages(new ArrayList<>());
    intent1.setName("Sorry, I can't understand you");
    intent1.setReply(reply);

    Message message = new Message();
    message.setIntents(new ArrayList<>());
    message.setMessage("Not all who wander are lost");
    message.setMessageId(123);

    IntentMessage intentMessage = new IntentMessage();
    intentMessage.setConfidence(0.95f);
    intentMessage.setIntent(intent1);
    intentMessage.setIntentMessageId(123);
    intentMessage.setMessage(message);

    Intent intent2 = new Intent();
    intent2.setIntentId(123);
    intent2.setMessages(new ArrayList<>());
    intent2.setName("Name");
    intent2.setReply(new Reply());

    Reply reply1 = new Reply();
    reply1.setIntent(intent2);
    reply1.setReplyId(123);
    reply1.setReplyMessage("Lower Confidence");

    Intent intent3 = new Intent();
    intent3.setIntentId(123);
    intent3.setMessages(new ArrayList<>());
    intent3.setName("Name");
    intent3.setReply(reply1);

    Message message1 = new Message();
    message1.setIntents(new ArrayList<>());
    message1.setMessage("Not all who wander are lost");
    message1.setMessageId(123);

    IntentMessage intentMessage1 = new IntentMessage();
    intentMessage1.setConfidence(0.9f);
    intentMessage1.setIntent(intent3);
    intentMessage1.setIntentMessageId(123);
    intentMessage1.setMessage(message1);

    ArrayList<IntentMessage> intentMessageList = new ArrayList<>();
    intentMessageList.add(intentMessage1);
    intentMessageList.add(intentMessage);

    Message message2 = new Message();
    message2.setIntents(intentMessageList);
    message2.setMessage("Not all who wander are lost");
    message2.setMessageId(123);
    IntentRecognizerService intentRecognizerService = mock(IntentRecognizerService.class);
    when(intentRecognizerService.getIntent(any())).thenReturn(intentMessageList);
    assertEquals(
        "Higher Confidence",
        (new BotReplyService(0.5f, intentRecognizerService))
            .getReply("Message Str")
            .getReplyMessage());
    verify(intentRecognizerService).getIntent(any());
  }

  @SneakyThrows
  @Test
  void testGetReplyNoReply() {

    Intent intent = new Intent();
    intent.setIntentId(123);
    intent.setMessages(new ArrayList<>());
    intent.setName("Sorry, I can't understand you");
    intent.setReply(new Reply());

    Reply reply = new Reply();
    reply.setIntent(intent);
    reply.setReplyId(123);
    reply.setReplyMessage("Sorry, I can't understand you");

    Intent intent1 = new Intent();
    intent1.setIntentId(123);
    intent1.setMessages(new ArrayList<>());
    intent1.setName("Sorry, I can't understand you");
    intent1.setReply(null);

    Message message = new Message();
    message.setIntents(new ArrayList<>());
    message.setMessage("Not all who wander are lost");
    message.setMessageId(123);

    IntentMessage intentMessage = new IntentMessage();
    intentMessage.setConfidence(0.95f);
    intentMessage.setIntent(intent1);
    intentMessage.setIntentMessageId(123);
    intentMessage.setMessage(message);

    ArrayList<IntentMessage> intentMessageList = new ArrayList<>();
    intentMessageList.add(intentMessage);

    Message message1 = new Message();
    message1.setIntents(intentMessageList);
    message1.setMessage("Not all who wander are lost");
    message1.setMessageId(123);
    IntentRecognizerService intentRecognizerService = mock(IntentRecognizerService.class);
    when(intentRecognizerService.getIntent(any())).thenReturn(intentMessageList);
    assertEquals(
        "Sorry, I can't understand you",
        (new BotReplyService(10.0f, intentRecognizerService))
            .getReply("Message Str")
            .getReplyMessage());
    verify(intentRecognizerService).getIntent(any());
  }
}
